<?php
/*
Plugin Name: Serg Ajax Plugin
Plugin URI: http://localhost
Description: Creation of widget with ajax test data
Version: 1.0
Author: Sergey
Author URI: 
*/
add_action( 'widgets_init', 'ajax_widget_test' );
function ajax_widget_test() {
	register_widget( 'AJAX_Widget_Test' );
}

//function for template

class AJAX_Widget_Test extends WP_Widget {

	function AJAX_Widget_Test() {
		$widget_ops  = array(
			'classname'   => 'test',
			'description' => __( 'It should be used for getting data about posts' )
		);
		$control_ops = array(
			'width'   => 200,
			'height'  => 350,
			'id_base' => __( 'ajax-widget-test' )
		);
		$this->WP_Widget( 'ajax-widget-test', 'Ajax Widget Test', $widget_ops, $control_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );
		$arg   = array(
			'post_type'      => $instance['post_type'],
			'posts_per_page' => $instance['quantity']
		);
		$query = new WP_Query( $arg );
		echo $before_widget;
		$context['post_type'] = $instance['post_type'];
		Timber::render( plugin_dir_path( __FILE__ ) . '/html/widget.twig', $context );
		self::show_links( $query );
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance              = $old_instance;
		$instance['quantity']  = intval( strip_tags( $new_instance['quantity'] ) );
		$instance['post_type'] = strip_tags( $new_instance['post_type'] );

		return $instance;
	}

	function form( $instance ) {
		$defaults                  = array(
			'quantity'  => '',
			'post_type' => 'post',
		);
		$instance                  = wp_parse_args( (array) $instance, $defaults );
		$post_types                = get_post_types();
		$context                   = Timber::get_context();
		$context['post_types']     = $post_types;
		$context['quantity_id']    = WP_Widget::get_field_id( 'quantity' );
		$context['quantity_name']  = WP_Widget::get_field_name( 'quantity' );
		$context['post_type_id']   = WP_Widget::get_field_id( 'post_type' );
		$context['post_type_name'] = WP_Widget::get_field_name( 'post_type' );
		$context['quantity_val']   = $instance['quantity'];
		Timber::render( plugin_dir_path( __FILE__ ) . "/html/inputs.twig", $context );
	}

	public static function show_links( $query ) {
		$context = Timber::get_context();
		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();
				$links[] = get_permalink();
			}
		}
		$context['links'] = $links;
		Timber::render( plugin_dir_path( __FILE__ ) . "/html/results.twig", $context );
	}

}

add_action( 'wp_enqueue_scripts', 'my_scripts_method' );
function my_scripts_method() {
	wp_enqueue_script( 'widget-ajax', plugin_dir_url( __FILE__ ) . "/js/widget-ajax.js" );
	wp_localize_script( 'widget-ajax', 'ajaxurl', array( 'url' => admin_url( 'admin-ajax.php' ) ) );
}

wp_register_style( 'style-ajax', plugin_dir_url( __FILE__ ) . "css/style.css" );
wp_enqueue_style( 'style-ajax' );
add_action( "wp_ajax_my_action", "ajax_my_action_func" );
add_action( "wp_ajax_nopriv_my_action", "ajax_my_action_func" );

function filter_where_title( $where = '' ) {
	$params = array();
	parse_str( $_POST['post'], $params );
	$title = $params['title'];
	$where .= "AND post_title LIKE '%$title%'";

	return $where;
}

function filter_where_date( $where = '' ) {
	$params = array();
	parse_str( $_POST['post'], $params );
	$date = $params['date'];
	$where .= "AND post_date >= '$date'";

	return $where;
}

function filter_where_total( $where = '' ) {
	$params = array();
	parse_str( $_POST['post'], $params );
	$date  = $params['date'];
	$title = $params['title'];
	$where .= "AND post_date >= '$date' AND post_title LIKE '%$title%'";

	return $where;
}

function ajax_my_action_func() {
	$params = array();
	parse_str( $_POST['post'], $params );
	$arg = array(
		'post_type' => $params['post_type'],
	);
	if ( $params['date'] !== '' && $params['title'] !== '' ) {
		add_filter( 'posts_where', 'filter_where_total' );
	} elseif ( $params['date'] !== '' && $params['title'] === '' ) {
		add_filter( 'posts_where', 'filter_where_date' );
	} elseif ( $params['title'] !== '' ) {
		add_filter( 'posts_where', 'filter_where_title' );
	}
	$query = new WP_Query( $arg );
	remove_filter( 'post_where', 'filter_where' );
	echo AJAX_Widget_Test::show_links( $query );
	wp_die();
}

?>
