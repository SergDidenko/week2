jQuery(document).ready(function() {
	jQuery('#post_form').submit(function(){
		var post = jQuery('#post_form').serialize();
		var data = {
		 	'action' : 'my_action',
		 	'post' : post
		};
		jQuery.post(ajaxurl.url, data,  function(response) {
		 	jQuery('#ajax-test-result').html(response);
		});
		return false;
	});
});